from datetime import date
from peewee import *
from db_tables.tables import Team,Player, Fixtures
from libs.general import rand
from array import array
import random
import sys

print sys.argv[1]
if (sys.argv[1]=="fixture"):
	#for purposes of fast development, I will assume the following teams are in the database.
	try:
		team_one = raw_input('Enter the name of the First Team: ')
		team_two = raw_input('Enter the name of the Second Team: ')
	except ValueError:
		print "The Value(s) supplied was incorrect.",sys.exc_info()[0]
		quit()

	#Query team from database just to check if its there
	try:
		team_one = Team.get(name = team_one)
		team_two =  Team.get(name = team_two)
	except Exception,e:
		print "An error occurred. Most likely the one of the teams selected do not exist."


	teamone_players = []
	teamtwo_players = []
	for person in Player.select().where(Player.team == team_one).order_by(Player.fitness):
		teamone_players.append(person.id)

	for person in Player.select().where(Player.team == team_two).order_by(Player.fitness):
		teamtwo_players.append(person.id)

	#the next block of code sets the tactics too
	attack = ['technical','aggressive']
	defense = ['wide','group']
	one_attack = random.choice(attack)
	one_defense = random.choice(defense)

	attack = ['technical','aggressive']
	defense = ['wide','group']
	two_attack = random.choice(attack)
	two_defense = random.choice(defense)
 	try:
		new_fixture = Fixtures.create(home_team = team_one,
									guest_team = team_two,
									competition = "Friendly",
									stage = "Friendly",
									played = False,
									team_one_attack = one_attack,
									team_one_defense = one_defense,
									team_two_attack = two_attack,
									team_two_defense = two_defense,
									p1t1 = teamone_players[1],
									p2t1 = teamone_players[2],
									p3t1 = teamone_players[3],
									p4t1 = teamone_players[4],
									p5t1 = teamone_players[0],
									p6t1 = teamone_players[6],
									p7t1 = teamone_players[7],
									p8t1 = teamone_players[8],
									p9t1 = teamone_players[9],
									p10t1 = teamone_players[10],
									p11t1 = teamone_players[11],
									p12t1 = teamone_players[5],
									p1t2 = teamtwo_players[1],
									p2t2 = teamtwo_players[2],
									p3t2 = teamtwo_players[3],
									p4t2 = teamtwo_players[0],
									p5t2 = teamtwo_players[5],
									p6t2 = teamtwo_players[6],
									p7t2 = teamtwo_players[7],
									p8t2 = teamtwo_players[8],
									p9t2 = teamtwo_players[9],
									p10t2 = teamtwo_players[10],
									p11t2 = teamtwo_players[11],
									p12t2 = teamtwo_players[4],
								)
		dir(new_fixture)
		print "The fixture:"+team_one.name+" vs "+team_two.name+" was successful created."
	except:
		print "An unexpected error occurred while trying to save the data." 
	raw_input()
elif (sys.argv[1]=="team"):

	try:
		team_name = raw_input("Team Name:")
		if (team_name==''):
			print "You cannot register a blank team"
			quit()
	except ValueError:
		print "The Value(s) supplied was incorrect.",sys.exc_info()[0]
		quit()

	team_name = str(team_name)
	#division - just allows generation of skills to vary according to division

	division = rand(1,5)
	if (division==1):
		max_seed = 15
		min_seed = 11
	elif (division==2):
		max_seed = 13
		min_seed = 9
	elif (division==3):
		max_seed = 11
		min_seed = 7
	elif (division==4):
		max_seed = 9
		min_seed = 5
	elif (division==5):
		max_seed = 7
		min_seed = 3
	else:
		print "Error: Invalid division."			

	team = Team.create(name = team_name,location= 'Nairobi')

	player = {}

	for i in range(0,12):
		i += 1
		first_name = random.choice(["Peter",            "John",            "Mathew",            "Mark",            "Anthony",            "Andrew",            "Edward",            "Wilson",            "Levy",            "Letser",            "George",            "Simon",            "Brian",            "Kevin",            "Steve",            "Kenedy",            "Harry",            "Japeth",            "Jack",            "Anthony",            "Bailey",            "Billy",            "Archie",            "Alexander",            "Adam",            "Benjamin",            "Charles",            "Daniel",            "David",            "Edward",            "Dominic",            "George",            "Harvey",            "Harrison",            "Jonathan",            "John",            "Joseph",            "Jacob",            "Isaac",            "Henry",            "Harry",            "James",            "Joshua",            "Matthew",            "Michael",            "Lucas"])
		last_name = random.choice([           "Njonjo",            "Nganga",            "Omollo",            "Kamau",            "Muema",            "Mwangi",            "Maina",            "Omundu",            "Njoroge",            "Wandoto",            "Kibe",            "Wachioi",            "Nyagah",            "Mwaura",            "Kamau",            "Mutiso",            "Ngungu",            "Mutali",            "Maina",            "Nganga",            "Mulinge",            "Thuku",            "Muhia",            "Mulinge",            "Gachanga",            "Chege",            "Chilemba",            "Chitundu",            "Chiumbo",            "Gachora",            "Gacoki",            "Gakere",            "Gakuru",            "Macaria ",            "Gatete",            "Gathee",            "Maina",            "Maitho",            "Makalani",            "Matunde",            "Mbogo",            "Maundu",            "Miano",            "Mathaathi",            "Matu",            "Migwi",            "Mpenda",            "Morani",            "Mugi",            "Muga",            "Mukiri",            "Muiru",            "Mukanda",            "Mugo",            "Muiruri",            "Mbui",            "Makori",            "Muenda",            "Mugendi","Chege ","Chomba ","Ciugaua","Gichere","Gachagua","Gachanja","Gachara","Gachii","Gakure","Gathaiya","Gathanja","Gathenya","Gathigira","Gathogo","Gathongo","Gathua","Gathuuri","Gakari","Karimi","Gitonga","Kimani","Kinuthia","Kenyatta ","Karanja","Kamotho","Kamande","Kagwa"])
		player[i] = Player.create(
									fname = first_name,lname = last_name,
									team = team ,
									weight = rand(76,136),
									height = rand(178,204),
									fitness = rand(min_seed,max_seed),
									stamina = rand(min_seed,max_seed),
									handling = rand(min_seed,max_seed),
									attack = rand(min_seed,max_seed),
									defense =rand(min_seed,max_seed),
									strength = rand(min_seed,max_seed),
									jumping = rand(min_seed,max_seed),
									speed = rand(min_seed,max_seed),
									technique =rand(min_seed,max_seed),
									agility = rand(min_seed,max_seed),
									kicking = rand(min_seed,max_seed),
									form = rand(1,100),
									aggression = rand(1,11),
									discipline = rand(1,11),
									energy = rand(1,100),
									leadership = rand(1,11),
									experience = rand(1,11),
									injured = False
								)



	print "The team was successful created. Division seed was: "+str(division)
	raw_input()