from peewee import *
from db_tables.tables import *


try:
    db.create_tables([Team,Player,Fixtures,Fixture_Statistics,Commentary])
    print 'Tables were created successfully.'
    raw_input()
except OperationalError:
	print 'Tables already exist.'
	raw_input()