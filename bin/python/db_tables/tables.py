from peewee import *


db = mysql_db = MySQLDatabase('sevens',user='root',password='')

class Team(Model):
	name = CharField()
	location= CharField()

	class Meta:
		database = db



class Player(Model):
	fname = CharField()
	lname = CharField()
	team = ForeignKeyField(Team, related_name='players')	
	weight = FloatField()
	height = FloatField()
	fitness = FloatField()
	stamina = FloatField()
	handling = FloatField()
	attack = FloatField()
	defense = FloatField()
	strength = FloatField()
	jumping = FloatField()
	speed = FloatField()
	technique = FloatField()
	agility = FloatField()
	kicking = FloatField()
	form = FloatField()
	aggression = FloatField()
	discipline = FloatField()
	energy = FloatField()
	leadership = FloatField()
	experience = FloatField()
	injured = BooleanField()

	class Meta:
		database = db	


class Fixtures(Model):
	home_team = ForeignKeyField(Team, related_name='home_team')
	guest_team = ForeignKeyField(Team, related_name='guest_team')
	competition = CharField()
	stage = CharField()
	team_one_attack = CharField()
	team_one_defense = CharField()
	team_two_attack = CharField()
	team_two_defense = CharField()
	played = BooleanField()
	p1t1 = IntegerField()
	p2t1 = IntegerField()
	p3t1 = IntegerField()
	p4t1 = IntegerField()
	p5t1 = IntegerField()
	p6t1 = IntegerField()
	p7t1 = IntegerField()
	p8t1 = IntegerField()
	p9t1 = IntegerField()
	p10t1 = IntegerField()
	p11t1 = IntegerField()
	p12t1 = IntegerField()
	p1t2 = IntegerField()
	p2t2 = IntegerField()
	p3t2 = IntegerField()
	p4t2 = IntegerField()
	p5t2 = IntegerField()
	p6t2 = IntegerField()
	p7t2 = IntegerField()
	p8t2 = IntegerField()
	p9t2 = IntegerField()
	p10t2 = IntegerField()
	p11t2 = IntegerField()
	p12t2 = IntegerField()


	class Meta:
		database = db	


class Fixture_Statistics(Model):
	fixture = ForeignKeyField(Fixtures, related_name ='stats')
	home_team_tries = IntegerField()
	home_team_conversions = IntegerField()
	home_team_total_points = IntegerField()
	home_team_rucks_won = IntegerField()
	home_team_mauls_won = IntegerField()
	home_team_possesion = IntegerField()
	home_team_scrum_won = IntegerField()
	home_team_errors = IntegerField()
	guest_team_tries = IntegerField()
	guest_team_conversions = IntegerField()
	gueste_team_total_points = IntegerField()
	guest_team_rucks_won = IntegerField()
	guest_team_mauls_won = IntegerField()
	guest_team_possesion = IntegerField()
	guest_team_scrum_won = IntegerField()
	guest_team_errors = IntegerField()
	
	class Meta:
		database = db	


class Commentary(Model):
	fixture= ForeignKeyField(Fixtures, related_name ='comments')
	commentary = CharField(max_length=855)

	class Meta:
		database = db	


