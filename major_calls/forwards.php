<?php
	function handle_forwards()
	{
		global $player, $ground, $gain, $minutes, $try;
		global $current, $number, $opponent, $team;
		global $tendancy_to_pass, $ruck_commit, $attack_method, $defence_method;
		
		if ( mt_rand( 1, 100 ) <= $tendancy_to_pass[ $current ] * ( 0.75 ) )
		{
			/********************************************************************************************************************/
			/* Goes back to tactics to see tactics set for the game. Forwards tend to attack more using
            strength as a major skill */
			//the player passes the ball, to the next one in position
			save_commentary($player[ $current ][ $number ][ 0 ] . " passes it to ");
			pass_ball();
			//$number = $number + 1;
			//average gain in ground is set to be between 100
			$gain = mt_rand( 2, 5 );
			//ground gained is updated
			determine_side();
			
			$ground = round($ground); 
			
			if($ground<=2){
				save_commentary($player[ $current ][ $number ][ 0 ] . " <span style=\"color:green;\"> and he just strides over the try line from $ground metre,It was an easy one for them, they played it to the  basics and got a reward for that. ");
				
					$player[$current][$number][13] = $player[$current][$number][13] + 1;
					$try[$current]=$try[$current]+5;
					call_try_restart();
							
							}else{
						save_commentary($player[ $current ][ $number ][ 0 ] . " and he recieves it at <i>$ground metres</i>. ");
						$minutes = $minutes - mt_rand(4,7);

					}

			
		}
		else
		{
		
		run_forwards();
		$minutes = $minutes - mt_rand(22,34);
			
		}

	}

	?>