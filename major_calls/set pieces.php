<?php
	function call_maul()
	{
		global $player, $ground, $gain, $minutes, $mauls_won;
		global $current, $number, $opponent, $team;
		global $tendancy_to_pass, $ruck_commit, $attack_method, $defence_method;
		
		//function calls for a maul, in the event a forward is brought down
		//first checks to see the endurance of a player to see how far he pushed
		
		$gain = ((mt_rand(145, mt_rand(245, 300)))/998) * $player[$current][$number][6];
		
		determine_side();
		
		//then we echo it out
		
		$ground = round($ground);	
		
		save_commentary(". <b>".$player[$current][$number][0]."</b> pushes on to <i>$ground metres</i> before turning back and calling a maul! . ");
		
		//number of players in a maul is equal to that off a ruck
		//here is where the code gets ugly PS SOrry for this but had to do it this way.
		//selection of opponent players to be in the ruck
		
		$total_strength      = 0;
		
		//refers to the total strength of the opponent defending the scrum.
		$opponent_enduarance = 0;
		
		////refers to the total endurance of the opponent defending the scrum.
		
		if ($ruck_commit[$opponent] == 1)
		{
			$first_opponent      = mt_rand(0, 2);
			$total_strength      = $player[$opponent][$first_opponent][7];
			$opponent_enduarance = $player[$opponent][$first_opponent][6];
		}

		elseif ($ruck_commit[$opponent] == 2)
		{
			$first_opponent  = mt_rand(0, 2);
			$second_opponent = mt_rand(3, 6);
			// add up their strength
			for ($i = 1; $i <= 2; $i++)
			{
				
				if ($i == 1)
				{
					$l = $first_opponent;
				}
				else
				{
					$l = $second_opponent;
				}

				$total_strength      = $total_strength + $player[$opponent][$l][7];
				$opponent_enduarance = $opponent_enduarance + $player[$opponent][$l][6];
			}

		}
		else
		{
			$first_opponent  = mt_rand(0, 2);
			$second_opponent = mt_rand(3, 6);
			$third_opponent  = mt_rand(3, 6);
			$total_strength  = 0;
			
			if ($third_opponent == $second_opponent && $third_opponent < 6)
			{
				$third_opponent = $third_opponent + 1;
			}
			else
			{
				$third_opponent = mt_rand(0, 2);
				
				if ($third_opponent == $first_opponent)
				{
					$third_opponent = $third_opponent + 1;
				}

			}

			// add up their strength
			for ($i = 1; $i <= 3; $i++)
			{
				
				if ($i == 1)
				{
					$o = $first_opponent;
				}

				elseif ($i == 2)
				{
					$o = $second_opponent;
				}

				elseif ($i == 3)
				{
					$o = $third_opponent;
				}

				$total_strength      = $total_strength + $player[$opponent][$o][7];
				$opponent_enduarance = $opponent_enduarance + $player[$opponent][$o][6];
			}

		}

		//*******************************************************************************
		//*******************************************************************************
		//call for current team.
		$total_strength2    = 0;
		$current_enduarance = 0;
		
		if ($ruck_commit[$current] == 1)
		{
			$first_current      = mt_rand(0, 2);
			$total_strength2    = $player[$opponent][$first_current][7];
			$current_enduarance = $player[$current][$first_current][6];
		}

		elseif ($ruck_commit[$current] == 2)
		{
			$first_current  = mt_rand(0, 2);
			$second_current = mt_rand(3, 6);
			// add up their strength
			for ($i = 1; $i <= 2; $i++)
			{
				
				if ($i == 1)
				{
					$q = $first_current;
				}
				else
				{
					$q = $second_current;
				}

				$total_strength2    = $total_strength2 + $player[$current][$q][7];
				$current_enduarance = $current_enduarance + $player[$current][$q][6];
			}

		}
		else
		{
			$first_current   = mt_rand(0, 2);
			$second_current  = mt_rand(3, 6);
			$third_current   = mt_rand(3, 6);
			$total_strength2 = 0;
			
			if ($third_current == $second_current && $third_current < 6)
			{
				$third_current = $third_current + 1;
			}
			else
			{
				$third_current = mt_rand(0, 2);
				
				if ($third_current == $first_current)
				{
					$third_current = $third_current + 1;
				}

			}

			// add up their strength
			for ($i = 1; $i <= 3; $i++)
			{
				
				if ($i == 1)
				{
					$p = $first_current;
				}

				elseif ($i == 2)
				{
					$p = $second_current;
				}

				elseif ($i == 3)
				{
					$p = $third_current;
				}

				$total_strength2    = $total_strength2 + $player[$current][$p][7];
				$current_enduarance = $current_enduarance + $player[$current][$p][6];
			}

		}

		
		if (mt_rand(1, 100) <= (($total_strength2 * 100) / 100))
		{
			//current team wins maul
			$gain = (mt_rand(0.125, mt_rand(0.199, 0.225))) * ($current_enduarance);
			determine_side();
			
			if ($ruck_commit[$current] == 3)
			{
				
				if ($third_current < 6)
				{
					$number = $third_current + 1;
				}
				else
				{
					$number = 6;
				}

			}

			elseif ($ruck_commit[$current] == 2)
			{
				
				if ($second_current < 6)
				{
					$number = $second_current + 1;
				}
				else
				{
					$number = 6;
				}

			}
			else
			{
				$number = mt_rand(3, 6);
			}

			save_commentary($team[$current] . " maintain possesion after that maul. The ball is gotten out to <b>" . $player[$current][$number][0] . "</b>");
		}
		else
		{
			$gain = (mt_rand(0.125, mt_rand(0.199, 0.225))) * ($opponent_enduarance);
			determine_side();
			
			if ($ruck_commit[$opponent] == 3)
			{
				
				if ($third_opponent < 6)
				{
					$number = $third_opponent + 1;
				}
				else
				{
					$number = 6;
				}

			}

			elseif ($ruck_commit[$opponent] == 2)
			{
				
				if ($second_opponent < 6)
				{
					$number = $second_opponent + 1;
				}
				else
				{
					$number = 6;
				}

			}
			else
			{
				$number = mt_rand(3, 6);
			}

			save_commentary($team[$opponent] . " snatch and win possesion after that maul, they quickly get the ball to <b>" . $player[$opponent][$number][0] . "</b>");
			change_of_possesion();
		}



		$mauls_won[$current]+=1;
	}

	



	function call_ruck()
	{
		global $player, $ground, $gain, $minutes,$rucks_won;
		global $current, $number, $opponent, $team;
		global $tendancy_to_pass, $ruck_commit, $attack_method, $defence_method;
			
		//set current situations in a variable
		$old_current =$current;



		if ($ruck_commit[$current] == 1)
		{
			$first_rucker = mt_rand(0, 6);
			
			if ($number == $first_rucker && $number < 6)
			{
				$first_rucker = $first_rucker + 1;
			}

			elseif ($number == $first_rucker && $number == 6)
			{
				$first_rucker = $first_rucker - 1;
			}

			$ruckers_current = $player[$current][$first_rucker][7];
		}

		elseif ($ruck_commit[$current] == 2)
		{
			$first_rucker = mt_rand(0, 6);
			//find first "rucker" for the current team
			$second_rucker = mt_rand(0, 6);
			//get second "rucker" for the current team
			
			if ($first_rucker == $second_rucker && ($first_rucker < 6 && $second_rucker > 0))
			{
				//it cant possibly be the same player so we choose the next player
				
				if (mt_rand(1, 2) == 1)
				{
					//randomly choose whether to increment or decrease to the next player, decrease is on the second player and increase is on the first player
					// this is not important but just bring some sense of diversity to the match
					$second_rucker = $first_rucker + 1;
				}
				else
				{
					//increment on first "rucker"
					$first_rucker = $second_rucker - 1;
				}

			}

			elseif ($first_rucker == $second_rucker && !($first_rucker < 6 && $second_rucker > 0))
			{
				//get back to selection because we cannot increment or decrease
				$first_rucker = mt_rand(0, 2);
				//get a forward
				$second_rucker = mt_rand(3, 6);
				//get a back
			}

			//now to check if any of them formed the ruck in the first place i.e any of them have the ball
			
			if ($number == $first_rucker && $second_rucker < 6)
			{
				$first_rucker = $second_rucker + 1;
			}

			elseif ($number == $first_rucker && $second_rucker > 0)
			{
				$first_rucker = $second_rucker - 1;
			}

			elseif ($number == $second_rucker && $first_rucker < 6)
			{
				$second_rucker = $first_rucker + 1;
			}

			elseif ($number == $second_rucker && $first_rucker > 0)
			{
				$second_rucker = $first_rucker - 1;
			}

			save_commentary(". <b>" . $player[$current][$first_rucker][0] . "</b> and <b>" . $player[$current][$second_rucker][0] . "</b> from " . $team[$current] . " join in to contest the ruck and hopefully maintain 
				possesion for their team. ");
			//oops almost forgot that one
			$ruckers_current = $player[$current][$first_rucker][7] + $player[$current][$second_rucker][7];
			//gather total strength
		}

		elseif ($ruck_commit[$current] == 3)
		{
			//three to join in the ruck
			$first_rucker = mt_rand(0, 2);
			//to be fair, a forward will be included in the ruck
			$second_rucker = mt_rand(3, 5);
			// a back
			
			if (mt_rand(1, 2) == 1)
			{
				//third one will be a forward
				$third_rucker = mt_rand(0, 2);
			}
			else
			{
				//the third one will be a back
				$third_rucker = mt_rand(3, 6);
			}

			//check that none of them made the ruck in the first place
			
			if ($first_rucker == $number)
			{
				$first_rucker = $first_rucker + 1;
			}

			elseif ($second_rucker == $number)
			{
				$second_rucker = $second_rucker + 1;
			}

			elseif ($third_rucker == $number)
			{
				$third_rucker = $second_rucker + 1;
			}

			//now check that none of them clash
			
			if ($first_rucker == $third_rucker && $third_rucker < 2)
			{
				$third_rucker = 2;
			}

			elseif ($first_rucker == $third_rucker && $third_rucker == 2)
			{
				$third_rucker = mt_rand(0, 1);
			}

			
			if ($second_rucker == $third_rucker && $third_rucker < 5)
			{
				$third_rucker = 6;
			}

			elseif ($second_rucker == $third_rucker && $third_rucker == 6)
			{
				$third_rucker = mt_rand(3, 5);
			}

			save_commentary($team[$current] . ".commit <b>" . $player[$current][$first_rucker][0] . "</b> and <b>" . $player[$current][$third_rucker][0] . "</b> 
			to join in and contest the ruck and hopefully maintain 
				possesion for their team. ");
			$ruckers_current = $player[$current][$first_rucker][7] + $player[$current][$second_rucker][7] + $player[$current][$third_rucker][7];
			//gather total strength
			//end of whatever is up here
		}

		//find variables for the opposition.. same process different index
		
		if ($ruck_commit[$opponent] == 1)
		{
			$first_rucker = mt_rand(0, 6);
			save_commentary(" , <b>" . $player[$opponent][$first_rucker][0] . "</b> will be the one to challenge the ruck for " . $team[$opponent]);
			$ruckers_opponent = $player[$opponent][$first_rucker][7];
		}

		elseif ($ruck_commit[$opponent] == 2)
		{
			$first_rucker = mt_rand(0, 6);
			//find first "rucker" for the current team
			$second_rucker = mt_rand(0, 6);
			//get second "rucker" for the current team
			
			if ($first_rucker == $second_rucker && ($first_rucker < 6 && $second_rucker > 0))
			{
				//it cant possibly be the same player so we choose the next player
				
				if (mt_rand(1, 2) == 1)
				{
					//randomly choose whether to increment or decrease to the next player, decrease is on the second player and increase is on the first player
					// this is not important but just bring some sense of diversity to the match
					$second_rucker = $first_rucker + 1;
				}
				else
				{
					//increment on first "rucker"
					$first_rucker = $second_rucker - 1;
				}

			}

			elseif ($first_rucker == $second_rucker && !($first_rucker < 6 && $second_rucker > 0))
			{
				//get back to selection because we cannot increment or decrease
				$first_rucker = mt_rand(0, 2);
				//get a forward
				$second_rucker = mt_rand(3, 6);
				//get a back
			}

			save_commentary("<b>" . $player[$opponent][$first_rucker][0] . "</b> and <b>" . $player[$opponent][$second_rucker][0] . "</b> from " . $team[$opponent] . " are set to contest the ruck, they look to snatch possesion from 
						" . $team[$current]);
			$ruckers_opponent = $player[$opponent][$first_rucker][7] + $player[$opponent][$second_rucker][7];
			//gather total strength
		}

		elseif ($ruck_commit[$opponent] == 3)
		{
			//three to join in the ruck
			$first_rucker = mt_rand(0, 2);
			//to be fair, a forward will be included in the ruck
			$second_rucker = mt_rand(3, 5);
			// a back
			
			if (mt_rand(1, 2) == 1)
			{
				//third one will be a forward
				$third_rucker = mt_rand(0, 2);
			}
			else
			{
				//the third one will be a back
				$third_rucker = mt_rand(3, 6);
			}

			//check that none of them made the ruck in the first place
			//now check that none of them clash
			
			if ($first_rucker == $third_rucker && $third_rucker < 2)
			{
				$third_rucker = 2;
			}

			elseif ($first_rucker == $third_rucker && $third_rucker == 2)
			{
				$third_rucker = mt_rand(0, 1);
			}

			
			if ($second_rucker == $third_rucker && $third_rucker < 5)
			{
				$third_rucker = 6;
			}

			elseif ($second_rucker == $third_rucker && $third_rucker == 6)
			{
				$third_rucker = mt_rand(3, 5);
			}

			save_commentary("<b>" . $player[$opponent][$first_rucker][0] . "</b> and <b>" . $player[$opponent][$third_rucker][0] . "</b> 
			join from the opposition to contest from their side ");
			$ruckers_opponent = $player[$opponent][$first_rucker][7] + $player[$opponent][$second_rucker][7] + $player[$opponent][$third_rucker][7];
			//gather total strength
			//end of whatever is up here for the opposition.
			//now check who gets possession
		}

		
		if ((mt_rand(1, 100) <= (($ruckers_current * 100) / $ruckers_current + $ruckers_opponent)))
		{
			//they maintain position after the ruck
			save_commentary("but " . $team[$current] . " maintain possesion after clearing out the opposition from contest.");
			$new_number=mt_rand(0,6);
			//this is because they have possesion to lose if they slack and wait for the scrum half
			
			if ($new_number==$number && $number<6)
			{
				$number=$number+1;
				//ensures the guy who formed the ruck isint the one getting it out in the fir
			}
			else
			{
				$number=$new_number;
			}

			save_commentary(". <b>".$player[$current][$number][0]."</b> continues on the phase! ");
		}
		else
		{
			save_commentary(" ,what a turn of event as " . $team[$opponent] . " snatch possesion immediately bringing to a stop their opposition's momentum.");
			change_of_possesion();
			$number = 4;
			save_commentary(". <b>".$player[$current][$number][0]."</b>, the scrum half begins a new phase for their team.");
		}



	
		$rucks_won[$current]+=1;

	}

	


	function call_scrum()
	{
		//the function calls for a scrum for when the ref sees fit to reset a play fairly but more often when someone knocks a ball
		global $player, $ground, $gain, $minutes,$scrums_won;
		global $current, $number, $opponent, $team;
		global $tendancy_to_pass, $ruck_commit, $attack_method, $defence_method;
		//first gather total strength of the players with the ball
		$scrum=array();
		//initiate a variable to hold total values to be tested in the scrum
		$scrum[1]=0;
		$scrum[2]=0;
		for ($i=0; $i<=2; $i++)
		{
			$scrum[$current]=$scrum[$current]+(($player[$current][$i][7]*60)/100)+(($player[$current][$i][11]*40)/100);
			//total strength for the current team with the ball
			$scrum[$opponent]=$scrum[$opponent]+(($player[$opponent][$i][7]*60)/100)+(($player[$opponent][$i][11]*40)/100);
			//total strength for the opposing team with the ball
			

				
		}

		$scrum_decider = mt_rand(1,100);
		$current_scrum = (($scrum[$current]*100)/($scrum[$current]+$scrum[$opponent]));
		if ($scrum_decider<=$current_scrum)
		{
			//the current team won the possesion of the scrum
			//here is where we calculate how far they pushed in the scrum 
			$number=3;
			save_commentary("<br> Both teams crash in for the scrum, ".$player[$current][1][0]." feeds in the ball, ".$team[$opponent]." get no chance with the ball as ".$team[$current]." maintain possesion after the scrum.<b>".$player[$current][$number][0]."</b> continues with the ball. <br>");
			
			$scrums_won[$current] += 1;
			
			
			
		}
		else
		{
			//here is where we calculate how far they pushed in the scrum or got pushed despite winning
			change_of_possesion();
			$number=3;
			save_commentary("<br>".$team[$opponent]." are squezzed by the power of ".$team[$current]." at the scrummage, looks like they are going to lose possesion,the ball switches sides, its  <b>".$player[$current][$number][0]."</b> to get the ball out.<br>");
			$scrums_won[$current] += 1;
		}

	}






	?>