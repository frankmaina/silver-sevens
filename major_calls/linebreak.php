<?php
//include('../minor_calls/minor_game_functions.php')
	function danger_offloading($variable)
	{
		
		global $player, $ground, $gain, $minutes, $try,$energy;
		global $current, $number, $opponent, $team;
		global $tendancy_to_pass, $ruck_commit, $attack_method, $defence_method;
		if ($current==1)
		{
			//the current team with the ball is team one..
			
			if($current==1 && (($ground+22)>=100))
			{
				
				//the team is attacking within the 22 so try to do an offload
				
				if(mt_rand(1,100)<=(($player[$current][$number][17]*100)/25))
				{
					//
					save_commentary(" <span style=\"color:green;\"> he managed to sucssesfully ofload He gains <i>$gain metres</i> but manages to do a brilliant offload to ");
					pass_ball();
					save_commentary(''.$player[$current][$number][0]. " at <i> $ground metres</i> ,who slowly strides in for a try at the centre, certainly nobody on the other side saw that coming and its a try for ".$team[$current]);
					$try[$current] = $try[$current] + 5;
					$player[$current][$number][13] = $player[$current][$number][13] + 1;
					call_try_restart();
				}
				else
				{
					save_commentary("he tries to get an offload but fails and the ball is gets lose from his grip and the ball drops  ");
					
					if (mt_rand(1,100)<=50)
					{
						save_commentary(" backwards, could the team memmbers of his team manage to form a ruck? ");
						
						if($energy[$current] > $energy[$opponent])
						{
							save_commentary(". . . . .  Oh! and yes they do, that was a critical one, they had to secure it");
	
						}
						elseif(($energy[$current] == $energy[$opponent]))
						{
							save_commentary(" oh! wait the ref doesnt like that one, he calls a ");

							call_scrum();
						}elseif(($energy[$current] == $energy[$opponent]))
						{
							save_commentary(" and its too late, the opponents manage to intercetpt that ball, ");
							change_of_possesion();
							$try_scorerer = mt_rand(4,6);
							save_commentary(''.$player[$current][$try_scorerer][0]."<span style=\"color:green;\"> sprints to the other end with the ball, good play!.");
							$try[$current] = $try[$current] + 5;
							$player[$current][$number][13] = $player[$current][$number][13] + 1;
							call_try_restart();							

						}
						

					}
					else
					{
						save_commentary("forwards,  the ref blows a whistle on that one, it will be a scrum!");
						call_scrum();
					}

				}

			}
			else
			{
				
					if ($variable=="maul"){

						$gain = (mt_rand(125, mt_rand(199, 225))) / 100 * $player[$current][$number][6];
						determine_side();
						$gain = round($gain);					
						save_commentary(" He gains $gain  metres before he is brought down at <i>$ground metres</i>, they quickly rush in to build a maul. ");
						call_maul();

					}else{

							
										save_commentary(" Oh that tackle must have hurt. A ruck is quickly formed.");
										
			
						call_ruck();

					}
			}

		}
		else
		{


			if($current==2 && (($ground-22)<=0))
			{
				//the team is attacking within the 22 so try to do an offload
				
				if(mt_rand(1,100)<=(($player[$current][$number][17]*100)/25))
				{
					//he managed to sucssesfully ofload
					save_commentary("<span style=\"color:green;\"> he  gains <i>$gain metres</i> but manages to do a brilliant offload to ");
					pass_ball();
					save_commentary(''.$player[$current][$number][0]. "  at <i>$ground metres</i> ,who slowly strides in for a try at the centre, certainly nobody on the other side saw that coming and its a try for ".$team[$current]);
					$try[$current] = $try[$current] + 5;
					$player[$current][$number][13] = $player[$current][$number][13] + 1;
					call_try_restart();
				}
				else
				{
					save_commentary("he tries to get an offload but fails and the ball is gets lose from his grip and the ball drops  ");
					
					if (mt_rand(1,100)<=50)
					{
						save_commentary(" backwards, could the team memmbers of his team manage to form a ruck? ");
						
						if($energy[$current] > $energy[$opponent])
						{
							save_commentary(". . . . .  Oh! and yes they do, that was a critical one, they had to secure it");
	
						}
						elseif(($energy[$current] == $energy[$opponent]))
						{
							save_commentary(" oh! wait the ref doesnt like that one, he calls a ");

							call_scrum();
						}elseif(($energy[$current] == $energy[$opponent]))
						{
							save_commentary(" and its too late, the opponents manage to intercetpt that ball, ");
							change_of_possesion();
							$try_scorerer = mt_rand(4,6);
							save_commentary(''.$player[$current][$try_scorerer][0]." <span style=\"color:green;\"> sprints to the other end with the ball, good play!.");
							$try[$current] = $try[$current] + 5;
							$player[$current][$number][13] = $player[$current][$number][13] + 1;
							call_try_restart();							

						}
						

					}
					else
					{
						save_commentary("forwards,  the ref blows a whistle on that one, it will be a scrum!");
						call_scrum();
					}

				}

			}
			else
			{
				
					if ($variable=="maul"){

						$gain = (mt_rand(125, mt_rand(199, 225))) / 100 * $player[$current][$number][6];
						determine_side();
						$gain = round($gain);					
						call_maul();

					}else{
										save_commentary(" Oh that tackle must have hurt. A ruck is quickly formed.");
										
			
						call_ruck();
					}
			}

		}

	}

	



	function line_break($defenders)
	{
		global $player, $ground, $gain, $minutes, $try;
		global $current, $number, $opponent, $team, $opponent_player;
		global $tendancy_to_pass, $ruck_commit, $attack_method, $defence_method;
		
		if ($defenders == 2)
		{
			//i really have no idea what this part of the code does
			$first_defender = mt_rand(0, 6);
			$second_defender = mt_rand(0, 6);
			
			if ($first_defender == $opponent_player && !($opponent_player > 2))
			{
				$first_defender = mt_rand(0, 2);
			}

			elseif ($second_defender == $opponent_player && !($opponent_player > 2))
			{
				$second_defender = mt_rand(0, 2);
			}

			
			if ($first_defender == $second_defender && $second_defender < 6)
			{
				$second_defender = $second_defender + 1;
			}

			elseif ($first_defender == $second_defender && $second_defender == 6)
			{
				$second_defender = $second_defender - mt_rand(1, 3);
			}

			//upto this part
			$next_move = mt_rand(1, 100);
			$gain_counter = 0;
			while ($player[$current][$number][7] >= $gain_counter)
			{
				$gain = mt_rand(1, 3);
				$gain_counter = $gain_counter + mt_rand(1, 5);
			}

			determine_side();
			save_commentary(''.$player[$opponent][$first_defender][0] . ", but while at <i>$ground metres</i>, " . $player[$opponent][$second_defender][0] . " rush across the field to get to him ");
			
			if ($next_move <= 60)
			{
				$total_strength = $player[$current][$number][7] + $player[$opponent][$first_defender][7] + $player[$opponent][$second_defender][7];
				
				if (mt_rand(1, 100) <= (($player[$current][$number][7] * 100) / $total_strength))
				{
					save_commentary(" but he remains unstoppable their despite efforts as they are both hurled to the ground with brute force, but can he make it to the try line? ");
					$attackers_ground = $ground;
					
					if ($current == 1)
					{
						$defenders_ground = $ground + 5;
					}
					else
					{
						$defenders_ground = $ground - 5;
					}

					$relative_ground = $attackers_ground - $defenders_ground;
					$relative_ground = abs($relative_ground);
					
					if (mt_rand(1, 3) <= 2)
					{
						$defender = 6;
						$attacker_speed = ($player[$current][$number][5] * (check_fitness($number, $current))) / 10;
						$defender_speed = ($player[$opponent][$defender][5] * (check_fitness($defender, $opponent))) / 10;
						$relative_speed = $attacker_speed + $defender_speed;
						$time = $relative_ground / $relative_speed;
						$gain = $attacker_speed * $time;
						$gain = round($gain);
						save_commentary(". He runs for another $gain metres before " . $player[$opponent][$defender][0] . " cathes up with him! ");
						
						if (mt_rand(1, 2) == 1)
						{
							$total = $player[$opponent][$defender][4] + $player[$current][$number][7];
							
							if (mt_rand(1, 100) <= ($player[$current][$number][7] * 100) / $total)
							{
								save_commentary(" <span style=\"color:green;\"> and He majestically fends off the full back and sprints straight for the posts!  ");
								$try[$current] = $try[$current] + 5;
								$player[$current][$number][13] = $player[$current][$number][13] + 1;
								call_try_restart();
							}
							else
							{

								danger_offloading("maul");
							}

						}
						else
						{
							$total = $player[$opponent][$defender][4] + $player[$current][$number][3];
							
							if (mt_rand(1, 100) <= ($player[$current][$number][3] * 100) / $total)
							{
								save_commentary(" <span style=\"color:green;\"> He steps in the and out again leaving the full back chasing after the clouds! ");
								$player[$current][$number][13] = $player[$current][$number][13] + 1;
								$try[$current] = $try[$current] + 5;
								call_try_restart();
							}
							else
							{
								danger_offloading("ruck");
							}

						}

					}
					else
					{
						save_commentary(" <span style=\"color:green;\"> and he is off to the posts, no one around to stop him, what a smile he has on his face after that play! ");
						$player[$current][$number][13] = $player[$current][$number][13] + 1;
						$try[$current] = $try[$current] + 5;
						call_try_restart();
					}

				}
				else
				{
					save_commentary("ooh he has not been able to shake off those defenders, and he is brought down like a rock. A ruck is formed. ");
					danger_offloading("ruck");
				}

			}

			elseif ($next_move > 60 && $next_move <= 80)
			{
				pass_ball();
				$gain = mt_rand(2, 5);
				determine_side();
				$ground = round($ground);
				save_commentary(". He gains good ground then passes the ball to " . $player[$current][$number][0] . " at <i>$ground metres</i>");
			}

			elseif ($next_move > 80 && $next_move <= 90)
			{
				save_commentary(". He will try to outrun the defenders, can he do it? ");
				
				if ($player[$opponent][$first_defender][5] > $player[$opponent][$second_defender][5])
				{
					save_commentary(''.$player[$opponent][$first_defender][0] . " will be the one to try and catch up to him.");
					$defender = $first_defender;
				}
				else
				{
					save_commentary(''.$player[$opponent][$second_defender][0] . " out runs his fellow team mate to be the one to take " . $player[$current][$number][0] . " down!.");
					$defender = $second_defender;
				}

				
				if (!(($ground - 22) <= 0) || !(($ground + 22) >= 100))
				{
					$attackers_ground = $ground;
					
					if ($current == 1)
					{
						$defenders_ground = $ground + 5;
					}
					else
					{
						$defenders_ground = $ground - 5;
					}

					$relative_ground = $attackers_ground - $defenders_ground;
					$relative_ground = abs($relative_ground);
					$attacker_speed = ($player[$current][$number][5] * (check_fitness($number, $current))) / 10;
					$defender_speed = ($player[$opponent][$defender][5] * (check_fitness($defender, $opponent))) / 10;
					$relative_speed = $attacker_speed + $defender_speed;
					$time = $relative_ground / $relative_speed;
					$gain = $attacker_speed * ($time * 1.2);
					$gain = round($gain);
					save_commentary(". He runs $gain metres before " . $player[$opponent][$defender][0] . " cathes up with him! Showing excelent fitness there. ");
					
					if (mt_rand(1, 2) == 1)
					{
						$total = $player[$opponent][$defender][4] + $player[$current][$number][7];
						
						if (mt_rand(1, 100) <= ($player[$current][$number][7] * 100) / $total)
						{
							save_commentary(" <span style=\"color:green;\">and He majestically fends off the full back and sprints straight for the posts!");
							$player[$current][$number][13] = $player[$current][$number][13] + 1;
							$try[$current] = $try[$current] + 5;
							call_try_restart();
						}
						else
						{
							$gain = (mt_rand(125, mt_rand(199, 225))) / 100 * $player[$current][$number][6];
							$gain = round($gain);
							determine_side();
							$ground = round($ground);
							
							danger_offloading("maul");
						}

					}
					else
					{
						$total = $player[$opponent][$defender][4] + $player[$current][$number][3];
						
						if (mt_rand(1, 100) <= ($player[$current][$number][3] * 100) / $total)
						{
							save_commentary(" <span style=\"color:green;\">and Oh! what a side step, the crowd certainly loved that as he heads off to the posts");
							$player[$current][$number][13] = $player[$current][$number][13] + 1;
							$try[$current] = $try[$current] + 5;
							call_try_restart();
						}
						else
						{
							
							danger_offloading("ruck");
						}

					}

				}

			}

			elseif ($next_move <= 100 && $next_move > 90)
			{
				$total = $player[$opponent][$second_defender][4] + $player[$opponent][$first_defender][4] + $player[$current][$number][3];
				
				if (mt_rand(1, 100) <= ($player[$current][$number][3] * 100) / $total)
				{
					save_commentary(" <span style=\"color:green;\">and Oh! what a side step, he manages to elude past Two defenders, that must be embarassing for " . $team[$opponent]);
					$player[$current][$number][13] = $player[$current][$number][13] + 1;
					$try[$current] = $try[$current] + 5;
					call_try_restart();
				}
				else
				{
					save_commentary(" and OHH! he is brought down by the combined efforts of " . $player[$opponent][$second_defender][0] . "  and 
			" . $player[$opponent][$first_defender][0] . ". The ref calls for release and a ruck is formed.");
					danger_offloading("ruck");
				}

			}

		}

		elseif ($defenders == 1)
		{
			
			if (mt_rand(1, 100) <= 70)
			{
				$first_defender = 6;
				
				if ($first_defender == $opponent_player)
				{
					$first_defender = mt_rand(0, 5);
				}

				$gain_counter = 0;
				while ($player[$current][$number][7] >= $gain_counter)
				{
					$gain = mt_rand(1, 3);
					$gain_counter = $gain_counter + mt_rand(6, 9);
				}

				$next_move = mt_rand(1,100);
				determine_side();
				save_commentary(''.$player[$opponent][$first_defender][0] . ", while at <i>$ground metres</i>, is the only man left standing between him and the try line, ");
				$defender = $first_defender;
				
				if ($next_move <= 60)
				{
					$total_strength = $player[$current][$number][7] + $player[$opponent][$first_defender][7];
					
					if (mt_rand(1, 100) <= (($player[$current][$number][7] * 100) / $total_strength))
					{
						save_commentary(" <span style=\"color:green;\"> and he remains unstoppable, a classic modern day Lomu, as he pushes forward to make his try! ");
						$player[$current][$number][13] = $player[$current][$number][13] + 1;
						$try[$current] = $try[$current] + 5;
						call_try_restart();
					}
					else
					{
						save_commentary(" but " . $player[$opponent][$first_defender][0] . " manages to bring him down, players from " . $team[$current] . " will be seeing no try if this guy is around. ");
						danger_offloading("ruck");
					}

				}
				else
				{
					
					if (mt_rand(1, 2) == 1)
					{
						$total = $player[$opponent][$defender][4] + $player[$current][$number][7];
						
						if (mt_rand(1, 100) <= ($player[$current][$number][7] * 100) / $total)
						{
							save_commentary(" <span style=\"color:green;\">and He majestically fends off the full back and sprints straight for the posts! ");
							$player[$current][$number][13] = $player[$current][$number][13] + 1;
							$try[$current] = $try[$current] + 5;
							call_try_restart();
						}
						else
						{
							$gain = (mt_rand(125, mt_rand(199, 225))) / 100 * $player[$current][$number][6];
							$gain = round($gain);
							determine_side();
							$ground = round($ground);
							save_commentary(" He gains $gain  metres before he is brought down at <i>$ground metres</i> and a maul is formed.");
							danger_offloading("maul");
						}

					}
					else
					{
						$total = $player[$opponent][$defender][4] + $player[$current][$number][3];
						
						if (mt_rand(1, 100) <= ($player[$current][$number][3] * 100) / $total)
						{
							save_commentary(" <span style=\"color:green;\">and Oh! what a side step, the crowd certainly loved that as he heads off to the posts  ");
							$player[$current][$number][13] = $player[$current][$number][13] + 1;
							$try[$current] = $try[$current] + 5;
							call_try_restart();
						}
						else
						{
							
							danger_offloading("ruck");
						}

					}

				}

			}
			else
			{
				$first_defender = mt_rand(0, 5);
				
				if ($first_defender == $opponent_player)
				{
					$first_defender = 6;
				}

				$gain_counter = 0;
				while ($player[$current][$number][7] >= $gain_counter)
				{
					$gain = mt_rand(1, 3);
					$gain_counter = $gain_counter + mt_rand(1, 5);
				}

				determine_side();
				$next_move = mt_rand(1,100);
				save_commentary(" but while at <i>$ground metres</i>," . $player[$opponent][$first_defender][0] . " is the only man left standing between him and the try line, ");
				
				if ($next_move <= 55)
				{
					$total_strength = $player[$current][$number][7] + $player[$opponent][$first_defender][7];
					
					if (mt_rand(1, 100) <= (($player[$current][$number][7] * 100) / $total_strength))
					{
						save_commentary(" <span style=\"color:green;\"> but he remains unstoppable, a classic modern day Lomu, as he pushes forward to make his try! ");
						$player[$current][$number][13] = $player[$current][$number][13] + 1;
						$try[$current] = $try[$current] + 5;
						call_try_restart();
					}
					else
					{
						save_commentary(" but " . $player[$opponent][$first_defender][0] . " manages to bring him down, players from " . $team[$current] . " will be seeing no try if this guy is around. ");
						danger_offloading("ruck");
					}

				}
				else
				{
					
					if (mt_rand(1, 2) == 1)
					{
						$total = $player[$opponent][$first_defender][4] + $player[$current][$number][7];
						
						if (mt_rand(1, 100) <= ($player[$current][$number][7] * 100) / $total)
						{
							save_commentary(" <span style=\"color:green;\">and He majestically fends off the full back and sprints straight for the posts!");
							$player[$current][$number][13] = $player[$current][$number][13] + 1;
							$try[$current] = $try[$current] + 5;
							call_try_restart();
						}
						else
						{
							$gain = (mt_rand(125, mt_rand(199, 225))) / 100 * $player[$current][$number][6];
							$gain = round($gain);
							determine_side();
							$ground = round($ground);
							save_commentary(" He gains $gain  metres before he is brought down at <i>$ground metres</i> and a maul is formed.");
							danger_offloading("maul");
						}

					}
					else
					{
						$total = $player[$opponent][$first_defender][4] + $player[$current][$number][3];
						
						if (mt_rand(1, 100) <= ($player[$current][$number][3] * 100) / $total)
						{
							save_commentary(" <span style=\"color:green;\">and Oh! what a side step, the crowd certainly loved that as he heads off to the posts ");
							$player[$current][$number][13] = $player[$current][$number][13] + 1;
							$try[$current] = $try[$current] + 5;
							call_try_restart();
						}
						else
						{
							
							danger_offloading("ruck");
						}

					}

				}

			}

		}

	}

	?>