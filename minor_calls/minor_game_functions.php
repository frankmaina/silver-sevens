<?php
    include( 'resources/commentary_functions.php' );
    include( 'major_calls/set pieces.php' );
    include( 'major_calls/forwards.php' );
    include( 'major_calls/linebreak.php' );
    include('major_calls/attack.php');
    include( 'major_calls/backs.php' );
    include( 'referee functions.php' );

    function print_players()
    {
        global $player;
        for ($i = 0; $i <= 4; $i++)
        {
            $jersey_number = $i + 1;
            echo $jersey_number . " " . $player[1][$i][0] . " ".$player[1][$i][1]." ".$player[1][$i][2]." ".$player[1][$i][3]." ".$player[1][$i][4]."<br>";
        }

        echo "<hr />";
        for ($i = 0; $i <= 4; $i++)
        {
            $jersey_number = $i + 1;
            echo $jersey_number . " " . $player[2][$i][0] . " ".$player[2][$i][1]." ".$player[2][$i][2]." ".$player[2][$i][3]." ".$player[2][$i][4]."<br>";
        }

    }

    
    function stimulate_game()
    {
        global $player, $played_scrum,$players_handed_the_ball;
        global $current, $opponent, $number;
        global $gain, $ground,$team;
        
        if (check_handling())
        {
            //the player didn't knock the match is to continue
            call_events();
            $played_scrum=0;
        }
        else
        {
            $ground = round($ground);
            echo " <b><span style=\"color:blue;\"> ";
            echo $player[$current][$number][0];
            knock_on();
            call_scrum();
            echo '</span>';
            $player[$current][$number][14] = $player[$current][$number][14]+1;
            $played_scrum = $played_scrum + 1;
        }

    }

    
    function show_time()
    {
        global $minutes;

        if ($minutes > 420){
            save_commentary("<br><b>" . "<span style=\"color:red;\">EXTRA TIME</span>" . "</b><br><br><br>");
        }
        else{
            save_commentary("<br><b>" . gmdate("i : s", $minutes) . "</b><br><br><br>");
        }
    }

    
    function change_of_possesion( )
    {
        global $current, $opponent;
        
        if ( $current == 1 )
        {
            $opponent = 1;
            $current  = 2;
        }

        //$current == 1
        else
        {
            $current  = 1;
            $opponent = 2;
        }

    }

    
    function kickoff( )
    {
        global $player;
        global $weather;
        global $current, $number;
        $kicking = ( $player[ $current ][ $number ][ 8 ] * 100 ) / 25;
        /* kicking is tested against a total of 100, if the random number falls under the range of the kicking ability then
    the kick was successful and possession was maintained, this is because not all kicks are always successful. */
        //the total is 25 so 25=100 what will be e.g.18... = (18*100)/25
        $success_kick = mt_rand( 1, 100 );
        
        if ( $success_kick <= $kicking )
        {
            //kick was successful and possession was maintained
            $result = 1;
            
            if (mt_rand(1,100)<=30)
            {
                $distance = mt_rand( 32, 46 );
            }
            else
            {
                $distance = mt_rand( 22, 29 );
            }

        }

        //$success_kick <= $kicking_testing
        else
        {
            $distance     = mt_rand( 24, 31 );
            $result = 0;
        }

        $inside_field = 0;
        $kicked_side = mt_rand(1,3);
        return array( $result, $distance, $inside_field, $kicked_side);
    }

    
    function determine_side( )
    {
        /* determines which side the ball is, in terms of the ground in metres. THis of course will not be displayed
    to the user during production but is only for development processes. Team One;s home side starts at 0 metres
    and they make a try at 100 metres. Team two has it vice versa. */
        global $current;
        global $ground;
        global $gain;
        
        if ( $current == 1 && ( 100 > ( $ground + $gain ) ) )
        {
            $ground = $ground + $gain;
        }

        //$current == 1 && ( 100 > ( $ground + $gain ) )
        elseif ( $current == 2 && ( ( $ground - $gain ) > 0 ) )
        {
            $ground = $ground - $gain;
        }

        //$current == 2 && ( ( $ground - $gain ) > 0 )
        else
        {
        }

        return $ground;
    }

    
    function after_kick( )
    {
        global $player, $team;
        global $current, $number;
        global $gain, $result,$kicked_side;
        global $ground;
        global $message_commentary;
        determine_side();
        
        if ( $result == 1 )
        {
            /* since the decision of the direction the ball will go to is often based on guessing which side has
        the weaker player ... I will leave this part to be random, in that whoever receives the ball is received
        randomly. */
            //possession is kept, hence receiver +-will be on the current side
            $number = mt_rand(0,6);
            
            if ( $number == 3 )
            {
                $number = 4;
            }

            //$number == 3
            echo "and what a good kick it was that lets <b>" . $player[ $current ][ $number ][ 0 ] . " </b> from " . $team[ $current ] . "</b> snatch it from the air and maintain 
                possession for his team. ";
        }

        //$result == 1
        else
        {
            
            if($kicked_side==1)
            {
                change_of_possesion();
                $number = mt_rand(1,3);
                echo "<b>" . $player[ $current ][ $number ][ 0 ] . "</b> from " . $team[ $current ] . " receives it after the kick, on the left side. ";
            }

            elseif($kicked_side==2)
            {
                change_of_possesion();
                $number = mt_rand(4,6);
                echo "<b>" . $player[ $current ][ $number ][ 0 ] . "</b> from " . $team[ $current ] . " receives it after the kick in the center.  ";
            }

            elseif($kicked_side==3)
            {
                change_of_possesion();
                $number = mt_rand(5,6);
                echo "<b>" . $player[ $current ][ $number ][ 0 ] . "</b> from " . $team[ $current ] . " receives it after the kick on the right side.  ";
            }

        }

        $message_commentary = field_position();
        echo " .It is received $gain metres out! at $ground metres $message_commentary ";
    }

    
    function pass_ball()
    {
        global $players_handed_the_ball;
        global $player, $ground, $gain, $minutes, $try;
        global $current, $number, $opponent, $team;
        global $tendancy_to_pass, $ruck_commit, $attack_method, $defence_method;
        
        if ($number == 0)
        {
            
            if(mt_rand(1,100)<=39)
            {
                //the player passes to a random player
                $number = mt_rand(2,6);
            }
            else
            {
                $number = 1;
            }

        }

        elseif($number == 1)
        {
            
            if(mt_rand(1,100)<=49)
            {
                //the player passes to a random player
                
                if(mt_rand(1,100)<=60)
                {
                    $number = mt_rand(2,6);
                }
                else
                {
                    $number = 4;
                }

            }
            else
            {
                $number = 2;
            }

        }

        elseif($number == 2)
        {
            
            if(mt_rand(1,100)<=49)
            {
                //the player passes to a random player
                
                if(mt_rand(1,100)<=70)
                {
                    $number = mt_rand(3,6);
                }
                else
                {
                    $number =  mt_rand(0,1);
                }

            }
            else
            {
                $number = 3;
            }

        }

        elseif($number == 3)
        {
            
            if(mt_rand(1,100)<=49)
            {
                //the player passes to a random player
                
                if(mt_rand(1,100)<=75)
                {
                    $number = mt_rand(4,6);
                }
                else
                {
                    $number =  mt_rand(0,2);
                }

            }
            else
            {
                $number = 4;
            }

        }

        elseif($number == 4)
        {
            
            if(mt_rand(1,100)<=49)
            {
                //the player passes to a random player
                
                if(mt_rand(1,100)<=75)
                {
                    $number = mt_rand(5,6);
                }
                else
                {
                    $number =  mt_rand(0,3);
                }

            }
            else
            {
                $number = 5;
            }

        }

        elseif($number == 5)
        {
            
            if(mt_rand(1,100)<=49)
            {
                //the player passes to a random player
                
                if(mt_rand(1,100)<=65)
                {
                    $number = 6;
                }
                else
                {
                    $number =  mt_rand(0,4);
                }

            }
            else
            {
                $number = 6;
            }

        }

        elseif($number == 6)
        {
            
            if(mt_rand(1,100)<=49)
            {
                //the player passes to a random player
                $number = mt_rand(0,5);
            }
            else
            {
                $number = $number = mt_rand(0,5);
            }

        }

    }

    
    function call_events( )
    {
        /* Checks player position and calculates how he plays accordingly. */
        global $player, $ground, $gain, $minutes, $try;
        global $current, $number, $opponent, $team;
        global $tendancy_to_pass, $ruck_commit, $attack_method, $defence_method;
        
        if ( $number == 0 || $number == 1 || $number == 2 )
        {
            //checks if the player is a forward
            $role = "forward";
            handle_forwards();
        }
        else
        {
            //the player is a back(9-15)
            $role = "Role: Back,";
            handle_backs();
        }

    }

    
    function check_try()
    {
        global $player, $ground, $gain, $minutes, $try;
        global $current, $number, $opponent, $team;
        global $tendancy_to_pass, $ruck_commit, $attack_method, $defence_method;
        
        if ($ground<=0 && $current==2)
        {
            $try[2] = $try[2] + 5;
        }

        elseif ($ground>=100 && $current==1)
        {
            $try[1] = $try[1] + 5;
        }

    }

    
    function call_conversion()
    {
        global $player, $ground, $gain, $minutes, $try, $weather ,$points;
        global $current, $number, $opponent, $team;
        global $tendancy_to_pass, $ruck_commit, $attack_method, $defence_method;
        //this function is called after a try has been made. It earns the team two extra points.
        $skill=$player[$current][3][8];
        //the kicking skill to be tested
        //sadly weather messes up most of life's moments 
        
        if ( $weather == "windy" )
        {
            $modifier = mt_rand (97,106);
        }

        elseif ( $weather == "sunny" )
        {
            $modifier = mt_rand (98,102);
        }

        //$weather == "sunny"
        elseif ( $weather == "stormy" )
        {
            $modifier = mt_rand (88,120);
        }

        //$weather == "stormy"
        elseif ( $weather == "dry" )
        {
            $modifier = mt_rand (103,113);
        }

        
        if (mt_rand(1,100)<=(($skill * $modifier)/24))
        {
            //the kick goes through 
            $try[$current] = $try[$current] + 2;
            echo "<b>".$player[$current][3][0]. "</b> gets one through the posts, adding two points for his team. Bringing the score to ".$team[1].' '.$try[1].' - '.$try[2].' '.$team[2].'. ';
            $points[$current][1] = $points[$current][1] + 1;
        }
        else
        {
            //conversion misses, nothing is  updated
            echo "<b>".$player[$current][3][0]. "</b> misses the shot to add two points for his team, the current score is  ".$team[1].' '.$try[1].' - '.$try[2].' '.$team[2].'. ';
            $points[$current][0] = $points[$current][0] + 1;
        }

    }

    
    function call_try_restart()
    {
        global $player, $ground, $gain, $minutes, $try;
        global $current, $number, $opponent, $team;
        global $tendancy_to_pass, $ruck_commit, $attack_method, $defence_method;
        call_conversion();
        echo "</span>";
        $ball2 = $player[$current][3][0];
       // echo "<hr /><br> ".$team[1]." ".$try[1]." - ".$try[2]." ".$team[2]."<br>";
        echo "<br></ hr> <br>";
        show_time();
        //the ground is first reset to half way
        $ground = 50;
        echo "<b>" . $ball2 . "</b> from ".$team[$current]."  will restart the match! " ;
        //then the try scorers kick it off to the oponent.
        list($result, $gain, $lineout) = kickoff();
        after_kick();
        echo "</ hr> <br>";
    }

    
    function check_fatigue()
    {
        global $player, $ground, $gain, $minutes, $try;
        global $current, $number, $opponent, $team;
        global $tendancy_to_pass, $ruck_commit, $attack_method, $defence_method;
        for ($i=0; $i<=6; $i++)
        {
            $fatigue = $player[1][$i][9];
            for ($p=0; $p<=7; $p++)
            {
                $skill = $p + 1;
                $player[1][$i][$skill] = $player[1][$i][$skill] - (mt_rand($fatigue,(2 * $fatigue))/100);
            }

        }

        for ($i=0; $i<=6; $i++)
        {
            $fatigue = $player[2][$i][9];
            for ($p=0; $p<=7; $p++)
            {
                $skill = $p + 1;
                $player[2][$i][$skill] = $player[2][$i][$skill] - (mt_rand($fatigue,(2 * $fatigue))/100);
            }

        }

    }

    
    function set_tactics()
    {
        global $tendancy_to_pass,$ruck_commit,$attack_method,$defence_method;
        
        if ($attack_method[1] == "technical")
        {
            $tendancy_to_pass[1] = mt_rand(60, 80);
        }
        else
        {
            $tendancy_to_pass[1] = mt_rand(35, 55);
        }

        
        if ($defence_method[1] == "wide")
        {
            $ruck_commit[1] = mt_rand(1, 2);
        }
        else
        {
            $ruck_commit[1] = mt_rand(2, 3);
        }

        
        if ($attack_method[2] == "technical")
        {
            $tendancy_to_pass[2] = mt_rand(55, 75);
        }
        else
        {
            $tendancy_to_pass[2] = mt_rand(35, 55);
        }

        
        if ($defence_method[2] == "wide")
        {
            $ruck_commit[2] = mt_rand(1, 2);
        }
        else
        {
            $ruck_commit[2] = mt_rand(2, 3);
        }

    }

    
    function check_energy()
    {
        global $player, $ground, $gain, $minutes, $try,$energy;
        global $current, $number, $opponent, $team;
        global $tendancy_to_pass, $ruck_commit, $attack_method, $defence_method;
        
        if (mt_rand(1,100)<=45)
        {
            if ($energy[$current]>$energy[$opponent])
            {
                return TRUE;
            }
            else
            {
                return FALSE;
            }
        }else{

            return FALSE;
        }

    }



    ?>